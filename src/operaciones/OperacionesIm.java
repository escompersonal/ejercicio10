/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package operaciones;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class OperacionesIm extends UnicastRemoteObject implements Operaciones{
    public OperacionesIm() throws RemoteException{
        super();
    }

    @Override
    public int sumaDato(int[] x) throws RemoteException{
        int res = 0;
        for(int i : x)
            res += i;
        return res;
    }

    @Override
    public double promedio(int[] x) throws RemoteException{
        double res = 0;
        for(int i : x){
            res += i;
        }
        return res/x.length;                
    }

    @Override
    public String mostrarDatos(int[] x) throws RemoteException{
        String cad = "";
        for(int i: x){
            cad += i+",";
        }
        return cad;
    }

    @Override
    public double de(int[] x) throws RemoteException{
        double d=0;
        double p = promedio(x);
        double d1 = 1/(x.length -1);
        for(int i : x){
            d += (i-p)*(i-p);
        }  
        
        return Math.sqrt(d1*d);
    }
    
    
}
